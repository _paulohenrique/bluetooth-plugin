using UnityEngine;

namespace PainfulSmile.Bluetooth
{
    public class AndroidPluginCallback : AndroidJavaProxy
    {
        public delegate void FindDeviceHandler(string deviceName, string deviceAddress, int deviceRssi);
        public delegate void FailedDeviceOperationHandler(string deviceAddress, int errorCode);
        public delegate void DeviceOperationHandler(string deviceAddress);
        public delegate void FailedOperationHandler(int errorCode);
        public delegate void UpdateCharacteristicHandler(string deviceAddress, byte[] value);
        public delegate void DisconnectDeviceHandler(string deviceAddress, bool connectionClosed);
        public delegate void MessageHandler(string message);

        public event FindDeviceHandler OnFindDevice;
        public event System.Action OnFinishScan;
        public event FailedOperationHandler OnScanFailed;
        public event DeviceOperationHandler OnConnectDevice;
        public event FailedDeviceOperationHandler OnConnectFailed;
        public event DisconnectDeviceHandler OnDisconnectDevice;
        public event FailedDeviceOperationHandler OnDisconnectFailed;
        public event UpdateCharacteristicHandler OnUpdateCharacteristic;
        public event MessageHandler OnLog;
        public event MessageHandler OnError;

        public AndroidPluginCallback() : base("com.painfulsmile.unityplugin.PluginCallback") { }

        private void InvokeOnFindDevice(string deviceName, string deviceAddress, int deviceRssi)
            => OnFindDevice?.Invoke(deviceName, deviceAddress, deviceRssi);

        private void InvokeOnFinishScan()
            => OnFinishScan?.Invoke();

        private void InvokeOnScanFailed(int errorCode)
            => OnScanFailed?.Invoke(errorCode);

        private void InvokeOnConnectDevice(string deviceAddress)
            => OnConnectDevice?.Invoke(deviceAddress);

        private void InvokeOnConnectFailed(string deviceAddress, int errorCode)
            => OnConnectFailed?.Invoke(deviceAddress, errorCode);

        private void InvokeOnDisconnectDevice(string deviceAddress, bool connectionClosed)
            => OnDisconnectDevice?.Invoke(deviceAddress, connectionClosed);

        private void InvokeOnDisconnectFailed(string deviceAddress, int errorCode)
            => OnDisconnectFailed?.Invoke(deviceAddress, errorCode);

        private void InvokeOnUpdateCharacteristic(string deviceAddress, string value)
            => OnUpdateCharacteristic?.Invoke(deviceAddress, System.Convert.FromBase64String(value));

        private void InvokeOnLog(string message)
            => OnLog?.Invoke(message);

        private void InvokeOnError(string errorMessage)
            => OnError?.Invoke(errorMessage);
    }
}
