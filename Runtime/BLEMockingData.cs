﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PainfulSmile.Bluetooth
{
    [CreateAssetMenu(fileName = nameof(BLEMockingData), menuName = "Data/" + nameof(BLEMockingData))]
    public class BLEMockingData : ScriptableObject
    {
        public delegate void DeviceBPMHandler(string address, int bpm);

        [System.Serializable]
        public class FakeDevice
        {
            public string Address;
            public string Name;
            public int CurrentBeatsPerMinute;
        }

        [SerializeField] private float _updateTime;
        [SerializeField] private List<FakeDevice> _devices;

        public IReadOnlyList<FakeDevice> Devices { get => _devices; }

        public IEnumerator UpdateDeviceCoroutine(string address, DeviceBPMHandler onRefreshDeviceBPM)
        {
            var device = _devices.Find((d) => d.Address == address);

            while (true)
            {
                yield return new WaitForSeconds(_updateTime);
                onRefreshDeviceBPM?.Invoke(device.Address, device.CurrentBeatsPerMinute);
            }
        }
    }
}
