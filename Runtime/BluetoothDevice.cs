﻿namespace PainfulSmile.Bluetooth
{
    public class BluetoothDevice
    {
        public string DeviceName;
        public string DeviceAddress;
        public int DeviceRssi;
    }
}
