﻿using System.Collections;
using UnityEngine;

namespace PainfulSmile.Bluetooth
{
    public static class PermissionManager
    {
        private static int GetSDKLevel()
        {
#if !UNITY_EDITOR && UNITY_ANDROID
        var clazz = AndroidJNI.FindClass("android/os/Build$VERSION");
        var fieldID = AndroidJNI.GetStaticFieldID(clazz, "SDK_INT", "I");
        var sdkLevel = AndroidJNI.GetStaticIntField(clazz, fieldID);
        return sdkLevel;
#endif

            return 0;
        }

        public static IEnumerator CheckAndroidPermissions(System.Action OnPermissionGranted, System.Action<string> OnPermissionDenied)
        {
            int sdkVersion = GetSDKLevel();
            bool canProceed = false;
            bool failed = false;
            string error = "";

            if (sdkVersion <= 30)
            {
                yield return CheckPermissionCoroutine("android.permission.BLUETOOTH", () =>
                {
                    canProceed = true;
                },
                () =>
                {
                    error += "BLUETOOTH permission denied!";
                    failed = true;
                });

                yield return new WaitUntil(() => canProceed || failed);
            }

            canProceed = false;
            yield return CheckPermissionCoroutine(UnityEngine.Android.Permission.FineLocation, () =>
            {
                canProceed = true;
            },
            () =>
            {
                error += $"\n{UnityEngine.Android.Permission.FineLocation} permission denied!";
                failed = true;
            });

            yield return new WaitUntil(() => canProceed || failed);

            if (sdkVersion > 30)
            {
                canProceed = false;
                yield return CheckPermissionCoroutine("android.permission.BLUETOOTH_SCAN", () =>
                {
                    canProceed = true;
                },
                () =>
                {
                    error += "\nBLUETOOTH_SCAN permission denied!";
                    failed = true;
                });

                yield return new WaitUntil(() => canProceed || failed);

                canProceed = false;
                yield return CheckPermissionCoroutine("android.permission.BLUETOOTH_CONNECT", () =>
                {
                    canProceed = true;
                },
                () =>
                {
                    error += "\nBLUETOOTH_CONNECT permission denied!";
                    failed = true;
                });

                yield return new WaitUntil(() => canProceed || failed);

            }

            if (!failed)
            {
                OnPermissionGranted?.Invoke();
            }
            else
            {
                OnPermissionDenied?.Invoke(error);
            }
        }

        private static void CheckPermission(string permission, System.Action OnPermissionGranted, System.Action OnPermissionDenied)
        {
            try
            {
                if (!UnityEngine.Android.Permission.HasUserAuthorizedPermission(permission))
                {
                    var callbacks = new UnityEngine.Android.PermissionCallbacks();

                    callbacks.PermissionGranted += (s) => OnPermissionGranted?.Invoke();
                    callbacks.PermissionDenied += (s) => OnPermissionDenied?.Invoke();
                    callbacks.PermissionDeniedAndDontAskAgain += (s) => OnPermissionDenied?.Invoke();

                    UnityEngine.Android.Permission.RequestUserPermission(permission, callbacks);
                }
                else
                {
                    OnPermissionGranted?.Invoke();
                }
            }
            catch (System.Exception e)
            {
                Debug.LogError(e);
            }
        }

        private static IEnumerator CheckPermissionCoroutine(string permission, System.Action OnPermissionGranted, System.Action OnPermissionDenied)
        {
            bool success = false;
            bool res = false;

            CheckPermission(permission, () =>
            {
                res = true;
                success = true;
            },
            () =>
            {
                res = true;
                success = false;
            });

            yield return new WaitUntil(() => res);

            if (success)
            {
                OnPermissionGranted?.Invoke();
            }
            else
            {
                OnPermissionDenied?.Invoke();
            }
        }
    }
}
