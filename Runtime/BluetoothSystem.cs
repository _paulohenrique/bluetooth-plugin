using System.Collections;
using UnityEngine;

//!UNITY_EDITOR && UNITY_ANDROID

namespace PainfulSmile.Bluetooth
{
    public static class BluetoothSystem
    {
        public enum ScanMode
        {
            LowPower,
            Balanced,
            LowLatency
        }

        public enum GattOperationStatus
        {
            Success = 0,
            ReadNotPermitted = 2,
            WriteNotPermitted = 3,
            InsufficientAuthentication = 5,
            RequestNotSupported = 6,
            InvalidOffset = 7,
            InsufficientAuthorization = 8,
            InvalidAttributeLength = 13,
            InsufficientEncryption = 15,
            Failure = 257,
            ConnectionCongested = 143
        }

        public enum ScanOperationErrorStatus
        {
            AlreadyStarted = 1,
            ApplicationRestrationFailed = 2,
            InternalError = 3,
            FeatureUnsupported = 4,
            OutOfHardwareResources = 5,
            ScanningToFrequently = 6
        }

#if UNITY_ANDROID
        private static AndroidJavaObject _pluginInstance;
        private static AndroidPluginCallback _callback;
#endif

        private static bool _initialized;

        public delegate void FindDevice(BluetoothDevice device);
        public delegate void DeviceHandler(string address);
        public delegate void RefreshBPM(string address, int bpm);
        public delegate void CharacteristicDataHandler(string address, byte[] data);

        public static event FindDevice OnFindDevice;
        public static event System.Action OnFinishScan;
        public static event System.Action OnScanFailed;
        public static event DeviceHandler OnConnectDevice;
        public static event DeviceHandler OnConnectFailed;
        //public static event DeviceHandler OnDeviceReconnected;
        public static event DeviceHandler OnDisconnectDevice;
        public static event DeviceHandler OnDeviceLostConnection;
        public static event DeviceHandler OnDisconnectFailed;
        public static event CharacteristicDataHandler OnUpdateCharacteristic;

        public static void Initialize()
        {
            if (_initialized)
            {
                return;
            }

            try
            {
#if UNITY_ANDROID
                _callback = new AndroidPluginCallback();

                _callback.OnFindDevice += HandleOnFindDevice;
                _callback.OnFinishScan += HandleOnFinishScan;
                _callback.OnConnectDevice += HandleOnConnectDevice;
                _callback.OnConnectFailed += HandleOnConnectFailed;
                _callback.OnUpdateCharacteristic += HandleOnUpdateCharacteristic;
                _callback.OnDisconnectDevice += HandleOnDisconnectDevice;
                _callback.OnDisconnectFailed += HandleOnDisconnectFailed;
                _callback.OnScanFailed += HandleOnScanFailed;
                _callback.OnLog += (message) => Debug.Log(message);
                _callback.OnError += (message) => Debug.LogError(message);

                AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaObject context = activity.Call<AndroidJavaObject>("getApplicationContext");

                _pluginInstance = new AndroidJavaObject("com.painfulsmile.unityplugin.PluginInstance");

                if (_pluginInstance == null)
                {
                    Debug.LogError("Failed to instantiate plugin class.");
                }

                _pluginInstance.Call("Initialize", context, _callback);
#endif
            }
            catch (System.Exception e)
            {
                Debug.LogException(e);
            }
        }

        private static void HandleOnUpdateCharacteristic(string deviceAddress, byte[] value)
        {
            OnUpdateCharacteristic?.Invoke(deviceAddress, value);
        }

        private static void HandleOnFinishScan()
        {
            OnFinishScan?.Invoke();
        }

        private static void HandleOnScanFailed(int errorCode)
        {
            OnScanFailed?.Invoke();
            Debug.LogError($"Failed to scan (code {errorCode}: {(((ScanOperationErrorStatus)errorCode).ToString())}");
        }

        private static void HandleOnFindDevice(string deviceName, string deviceAddress, int deviceRssi)
        {
            OnFindDevice?.Invoke(new BluetoothDevice()
            {
                DeviceAddress = deviceAddress,
                DeviceName = deviceName,
                DeviceRssi = deviceRssi
            });
        }

        private static void HandleOnDisconnectFailed(string deviceAddress, int errorCode)
        {
            OnDisconnectFailed?.Invoke(deviceAddress);
            Debug.LogError($"Failed to disconnect {deviceAddress} (code {errorCode}: {(((GattOperationStatus)errorCode).ToString())}");
        }

        private static void HandleOnDisconnectDevice(string deviceAddress, bool connectionClosed)
        {
            if (connectionClosed)
            {
                OnDisconnectDevice?.Invoke(deviceAddress);
            }
            else
            {
                OnDeviceLostConnection?.Invoke(deviceAddress);
            }
        }

        private static void HandleOnConnectFailed(string deviceAddress, int errorCode)
        {
            OnConnectFailed?.Invoke(deviceAddress);
            Debug.LogError($"Failed to connect to {deviceAddress} (code {errorCode}: {(((GattOperationStatus)errorCode).ToString())}");
        }

        private static void HandleOnConnectDevice(string deviceAddress)
        {
            Debug.Log($"HandleOnConnectDevice -> {deviceAddress}");
            OnConnectDevice?.Invoke(deviceAddress);
        }

        private static IEnumerator RunAfterTimeCoroutine(System.Action action, float time)
        {
            yield return new WaitForSecondsRealtime(time);
            action?.Invoke();
        }

        public static void StartScan(string serviceUUid)
        {
#if UNITY_ANDROID
            _pluginInstance.Call("ScanForDevices", serviceUUid, (int)ScanMode.Balanced);
#endif
        }

        public static void StopScan()
        {
#if UNITY_ANDROID
            _pluginInstance.Call("StopScan");
#endif
        }

        public static void ConnectToDevice(string deviceAddress, string serviceUUid, string characteristicUUid)
        {
#if UNITY_ANDROID
            _pluginInstance.Call("ConnectToDevice", deviceAddress, serviceUUid, characteristicUUid, 256);
#endif
        }

        public static void DisconnectDevice(string deviceAddress)
        {
#if UNITY_ANDROID
            _pluginInstance.Call("DisconnectDevice", deviceAddress);
#endif
        }

        public static void DisconnectAll()
        {
#if UNITY_ANDROID
            _pluginInstance.Call("DisconnectAll");
#endif
        }
    }
}
